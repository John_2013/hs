<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<aside class="asideHeader">
    <ul class="vertFlexMenu">
        <li><?=html::a('Главная','/site/index')?></li>
        <li><?=html::a('HTML/CSS','/site/index')?></li>
        <li><?=html::a('Клиентские ЯП','/site/index')?></li>
        <li><?=html::a('Серверные ЯП','/site/index')?></li>
        <li><?=html::a('Фреймворки','/site/index')?></li>
    </ul>



</aside>
<div class="content">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>
    <?= $content ?>
</div>
<aside class="asideFooter">
    <?

    if(yii::$app->user->isGuest){
        echo "
<ul>
    <li>".html::a('site/login','Войти')."</li>
    <li>".html::a('site/reg','Зарегистрироваться')."</li>
</ul>
        ";
    }

    ?>
        <p class="">&copy; My Company <?= date('Y') ?></p>

        <p class=""><?= Yii::powered() ?></p>
</aside>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
